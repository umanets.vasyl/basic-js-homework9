function createUl(array, domElem = document.body) {
  let ulElem = document.createElement("ul");
  domElem.append(ulElem);
  array.map((item) => {
    let li = document.createElement("li");
    if (Array.isArray(item)) {
      createUl(item, li);
      ulElem.append(li);
    } else {
      li.innerText = item;
      ulElem.append(li);
    }
  });
}

list = [
  "Kharkiv",
  "Kyiv",
  ["Borispol", ["Obolon", "Darnytsya"]],
  "Odesa",
  "Lviv",
  "Dnipro",
];

createUl(list);

let timeleft = 3;

let downloadTimer = setInterval(function () {
  if (timeleft <= 0) {
    clearInterval(downloadTimer);
    document.body.innerHTML = "";
  } else {
    document.getElementById("countdown").innerHTML =
      timeleft + " seconds remaining";
  }
  timeleft--;
}, 1000);
